import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { CategoryPage } from '../category/category';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
  	public navCtrl: NavController,
  	public menuCtrl: MenuController) {

  }

  ionViewDidLoad() {
  	this.menuCtrl.enable(true);
  }

  goPage(title) {
  	this.navCtrl.push(CategoryPage, { name: title });
  }

}
