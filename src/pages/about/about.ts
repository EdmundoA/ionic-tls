import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

	public title = '';

	constructor(
	    public navCtrl: NavController,
	    public navParams: NavParams) {
	}

	ionViewDidLoad() {
	    this.title = this.navParams.get('name');
	    console.log('ionViewDidLoad CategoryPage');
	}

}
